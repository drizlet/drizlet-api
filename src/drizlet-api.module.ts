import { NgModule, ModuleWithProviders } from '@angular/core';
//import { AcademyComponent } from './components/academy-component';

import { DrizletProvider } from './providers/main';

import { AdminAuthService } from './providers/AdminAuthService';
import { RetailerAuthService } from './providers/RetailerAuthService';
import { ChainAuthService } from './providers/ChainAuthService';
import { CustomerAuthService } from './providers/CustomerAuthService';

import { IonicModule } from 'ionic-angular';
 
@NgModule({
    imports: [
        // Only if you use elements like ion-content, ion-xyz...
        IonicModule
    ],
    declarations: [
        // declare all components that your module uses
        //AcademyComponent
    ],
    exports: [
        // export the component(s) that you want others to be able to use
        //AcademyComponent
    ]
})
export class DrizletAPIModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: DrizletAPIModule,
            providers: [
                DrizletProvider,
                AdminAuthService,
                RetailerAuthService,
                ChainAuthService,
                CustomerAuthService
            ]
        };
    }
}