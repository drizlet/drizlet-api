import { Injectable } from '@angular/core';
 
@Injectable()
export class DrizletProvider {
  description() {
    return 'Provides Drizlet API';
  }
}