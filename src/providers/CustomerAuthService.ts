import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";

import {HelperService} from "./HelperService";

import {API_ENDPOINT} from "../providers/constants"
import 'rxjs/add/operator/map';

@Injectable()
export class CustomerAuthService {
  
  constructor(private http: Http, private helperService: HelperService) {
    this.http = http;
  }

  checkUsername (u) {
    if (!u) {
      return 'Username not entered';
    }
    else if (u.length < 8) {
      return 'Username should be at least 8 characters long';
    }
    else{
      var l = u.length;
      var i = 0;
      for(i = 0; i < l; i++){
        switch(u[i]){
          case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': 
          case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z':
          case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M':
          case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z':
          case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': break;
          default: return 'Username should be only alphabets and numbers!';
        }
      }
      return null;
    }
  };

  register(user) {
    var isloginEmpty = false;
    var isPasswordEmpty = false;
    var isEmailEmpty = false;
    
    if(!user.username || user.username === '' || user.username.length === 0){
      isloginEmpty = true;
    }

    if(!user.password || user.password === '' || user.password.length === 0){
      isPasswordEmpty = true;
    }
    
    if(!user.email || user.email === '' || user.email.length === 0){
      isEmailEmpty = true;
    }
    
    return new Promise((resolve, reject) => {
      if(this.checkUsername(user.username)){
        reject(this.checkUsername(user.username));
      }
      else if(isloginEmpty || isPasswordEmpty || isEmailEmpty){
        reject('Please enter required fields!');
      }
      else {
        this.http.post(API_ENDPOINT.url + '/customer/signup', user, this.getOptions())
        .map(res => {
          return res.json();
        })
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  login(user) {
    var isloginEmpty = false;
    var isPasswordEmpty = false;
    if(!user.login || user.login === '' || user.login.length === 0){
      isloginEmpty = true;
    }

    if(!user.password || user.password === '' || user.password.length === 0){
      isPasswordEmpty = true;
    }
    
    return new Promise((resolve, reject) => {
      if(isloginEmpty && isPasswordEmpty){
        reject('Username and password not entered!');
      }
      else if(isloginEmpty){
        reject('Username not entered!');
      }
      else if(isPasswordEmpty){
        reject('Password not entered!');
      }
      else {
        
        this.http.post(API_ENDPOINT.url + '/customer/login', user)
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('customer', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  login_with_auth(user) {
    return new Promise((resolve, reject) => {
      if(!user.authToken || user.authToken === '' || user.authToken.length === 0){
        reject('No authentication found!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/customer/login_auth', this.getOptionsWithAuth(user.authToken))
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('customer', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  login_with_token() {
    return new Promise((resolve, reject) => {
      this.helperService.loadUserCredentials();
      if(!this.getToken() || this.getToken() === '' || this.getToken().length === 0){
        reject('No authentication found!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/customer/login_auth', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('customer', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  reset_password(login) {
    return new Promise((resolve, reject) => {
      if(!login || login.length === 0){
        reject('You did not enter anything!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/customer/reset_pass/'+login)
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  send_verification(login) {
    return new Promise((resolve, reject) => {
      if(!login || login.length === 0){
        reject('You did not enter anything!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/customer/send_verify_token/'+login)
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  getInfo() {
    return new Promise((resolve, reject) => {
      if(!this.getToken()){
        reject('token does not exist!');
      }
      this.http.get(API_ENDPOINT.url + '/customer/info', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result.data);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getUsers() {
    return new Promise((resolve, reject) => {
      if(!this.getToken()){
        reject('token does not exist!');
      }
      this.http.get(API_ENDPOINT.url + '/customer/get_users', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result.data);
        } else {
          reject(result.data);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getUserInfo(user_id) {
    return new Promise((resolve, reject) => {
      if(!user_id){
        reject('User id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/customer/get_user_info/'+user_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  update(user) {
    return new Promise((resolve, reject) => {
      var isloginEmpty = false;
      var isEmailEmpty = false;
      
      if(!user.username || user.username === '' || user.username.length === 0){
        isloginEmpty = true;
      }

      if(!user.email || user.email === '' || user.email.length === 0){
        isEmailEmpty = true;
      }
    
      if(isloginEmpty || isEmailEmpty){
        reject('Please enter login and emial!');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/customer/update', user, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  changePassword(passwords) {
    return new Promise((resolve, reject) => {
      var isOldEmpty = false;
      var isNewEmpty = false;
      var isRepeatEmpty = false;
      
      if(!passwords.old || passwords.old === '' || passwords.old.length === 0){
        isOldEmpty = true;
      }

      if(!passwords.new || passwords.new === '' || passwords.new.length === 0){
        isNewEmpty = true;
      }

      if(!passwords.repeat || passwords.repeat === '' || passwords.repeat.length === 0){
        isRepeatEmpty = true;
      }

      if(isOldEmpty || isNewEmpty || isRepeatEmpty){
        reject('Please enter all details.');
      }
      else if(passwords.new !== passwords.repeat){
        reject('New passwords do not match.');
    }
      else if(passwords.old === passwords.repeat){
        reject('Same password entered.');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/customer/change_password', passwords, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }


// ==============================================================
// =                       Retailer                             =
// ==============================================================
  addNewRetailer(retailer) {
    return new Promise((resolve, reject) => {
      if(!retailer){
        reject('Error in Retailer data.');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/customer/add_new_retailer', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  getRetailers() {
    return new Promise((resolve, reject) => {
        this.http.get(API_ENDPOINT.url + '/customer/get_retailers', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  getRetailerInfo(retailer_id) {
    return new Promise((resolve, reject) => {
      if(!retailer_id){
        reject('Retailer id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/customer/get_retailer_info/'+retailer_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
  updateRetailerProfile(retailer) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/customer/update_retailer_profile', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  updateRetailerDeals(retailer) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/customer/update_retailer_deals', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  changeRetailerPassword(retailer){
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/customer/change_retailer_password', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  }
// ==============================================================
// =                        Chains                              =
// ==============================================================

  getChainInfo(chain_id) {
    return new Promise((resolve, reject) => {
      if(!chain_id){
        reject('Chain id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/customer/get_chain_info/' + chain_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
// ==============================================================
// =                       Drizlet Card                         =
// ==============================================================
  
  getDrizletCardInfo() {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/customer/get_drizletcard_info', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  
// ==============================================================
// =                     Transactions                           =
// ==============================================================


  getAllTransactions(deals_type) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/customer/get_all_transactions/' + deals_type, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getTransactionsInYear(year, deals_type) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/customer/get_transactions_in_year/' + year + '/' + deals_type, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getTransactionsInMonth(year, month, deals_type) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/customer/get_transactions_in_month/' + year + '/' + month + '/' + deals_type, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
// ==============================================================
// =                          Misc                              =
// ==============================================================

  getToken(): any{
    return this.helperService.getToken();
  }

  getOptions(){
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.getToken()
    });
    return new RequestOptions({ headers: headers });
  }

  getOptionsWithAuth(authToken){
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });
    return new RequestOptions({ headers: headers });
  }

  logout(){
    this.helperService.logout();
  }
}
