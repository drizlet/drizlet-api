import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";

import {HelperService} from "./HelperService";

import {API_ENDPOINT} from "../providers/constants"
import 'rxjs/add/operator/map';


@Injectable()
export class ChainAuthService {

  constructor(private http: Http, private helperService: HelperService) {
    this.http = http;
  }

// ==============================================================
// =                        Chain                           =
// ==============================================================

  register(user) {
    var isloginEmpty = false;
    var isPasswordEmpty = false;
    var isEmailEmpty = false;
    
    if(!user.username || user.username === '' || user.username.length === 0){
      isloginEmpty = true;
    }

    if(!user.password || user.password === '' || user.password.length === 0){
      isPasswordEmpty = true;
    }
    
    if(!user.email || user.email === '' || user.email.length === 0){
      isEmailEmpty = true;
    }
    
    return new Promise((resolve, reject) => {
      if(isloginEmpty || isPasswordEmpty || isEmailEmpty){
        reject('Please enter required fields!');
      }
      else {
        this.http.post(API_ENDPOINT.url + '/chain/signup', user, this.getOptions())
        .map(res => {
          return res.json();
        })
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  login(user) {
    var isloginEmpty = false;
    var isPasswordEmpty = false;
    if(!user.login || user.login === '' || user.login.length === 0){
      isloginEmpty = true;
    }

    if(!user.password || user.password === '' || user.password.length === 0){
      isPasswordEmpty = true;
    }
    
    return new Promise((resolve, reject) => {
      if(isloginEmpty && isPasswordEmpty){
        reject('Username and password not entered!');
      }
      else if(isloginEmpty){
        reject('Username not entered!');
      }
      else if(isPasswordEmpty){
        reject('Password not entered!');
      }
      else {
        console.log('calling ' + API_ENDPOINT.url);
        
        this.http.post(API_ENDPOINT.url + '/chain/login', user)
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('chain', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
  login_with_auth(user) {
    return new Promise((resolve, reject) => {
      if(!user.authToken || user.authToken === '' || user.authToken.length === 0){
        reject('No authentication found!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/chain/login_auth', this.getOptionsWithAuth(user.authToken))
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('chain', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  login_with_token() {
    return new Promise((resolve, reject) => {
      this.helperService.loadUserCredentials();
      if(!this.getToken() || this.getToken() === '' || this.getToken().length === 0){
        reject('No authentication found!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/chain/login_auth', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('chain', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
  getInfo() {
    return new Promise((resolve, reject) => {
      if(!this.getToken()){
        reject('token does not exist!');
      }
      this.http.get(API_ENDPOINT.url + '/chain/info', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result.data);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getUsers() {
    return new Promise((resolve, reject) => {
      if(!this.getToken()){
        reject('token does not exist!');
      }
      this.http.get(API_ENDPOINT.url + '/chain/get_users', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result.data);
        } else {
          reject(result.data);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getUserInfo(user_id) {
    return new Promise((resolve, reject) => {
      if(!user_id){
        reject('User id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/chain/get_user_info/'+user_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  update(user) {
    return new Promise((resolve, reject) => {
      var isloginEmpty = false;
      var isPasswordEmpty = false;
      var isEmailEmpty = false;
      var isdealsTypeEmpty = false;

      if(!user.username || user.username === '' || user.username.length === 0){
        isloginEmpty = true;
      }

      if(!user.password || user.password === '' || user.password.length === 0){
        isPasswordEmpty = true;
      }
      
      if(!user.email || user.email === '' || user.email.length === 0){
        isEmailEmpty = true;
      }
    
      if(!user.deals_type || user.deals_type === '' || user.email.deals_type === 0){
        isdealsTypeEmpty = true;
      }
    
      if(isloginEmpty || isPasswordEmpty || isEmailEmpty || isdealsTypeEmpty){
        reject('Please enter required fields!');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/chain/update', user, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  changePassword(passwords) {
    return new Promise((resolve, reject) => {
      var isOldEmpty = false;
      var isNewEmpty = false;
      var isRepeatEmpty = false;
      
      if(!passwords.old || passwords.old === '' || passwords.old.length === 0){
        isOldEmpty = true;
      }

      if(!passwords.new || passwords.new === '' || passwords.new.length === 0){
        isNewEmpty = true;
      }

      if(!passwords.repeat || passwords.repeat === '' || passwords.repeat.length === 0){
        isRepeatEmpty = true;
      }

      if(isOldEmpty || isNewEmpty || isRepeatEmpty){
        reject('Please enter all details.');
      }
      else if(passwords.new !== passwords.repeat){
        reject('New passwords do not match.');
    }
      else if(passwords.old === passwords.repeat){
        reject('Same password entered.');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/chain/change_password', passwords, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

// ==============================================================
// =                        Retailers                           =
// ==============================================================

  addNewRetailer(retailer) {
    return new Promise((resolve, reject) => {
      if(!retailer){
        reject('Error in Retailer data.');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/chain/add_new_retailer', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  getRetailers() {
    return new Promise((resolve, reject) => {
        this.http.get(API_ENDPOINT.url + '/chain/get_retailers', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  getRetailerInfo(retailer_id) {
    return new Promise((resolve, reject) => {
      if(!retailer_id){
        reject('Retailer id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/chain/get_retailer_info/'+retailer_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  updateRetailerProfile(retailer) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/chain/update_retailer_profile', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  updateRetailerDeals(retailer) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/chain/update_retailer_deals', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

// ==============================================================
// =                         Salesmen                           =
// ==============================================================

  getSalesmen() {
    return new Promise((resolve, reject) => {
        this.http.get(API_ENDPOINT.url + '/chain/get_salesmen', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
    });
  };

  getSalesmanInfo(salesman_id) {
    return new Promise((resolve, reject) => {
      if(!salesman_id){
        reject('Salesman id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/chain/get_salesman_info/'+salesman_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

// ==============================================================
// =                     Transactions                           =
// ==============================================================

  getChainTransactionsHistory(input) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_chain_transactions_history/'+ 
        input.transactionType + '/' +
        input.pageNum + '/' + 
        input.transactionsPerPage
        , this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getRetailerTransactionsHistory(input) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_retailer_transactions_history/'+ 
        input.selectedRetailerId + '/' +
        input.transactionType + '/' +
        input.pageNum + '/' + 
        input.transactionsPerPage
        , this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getSalesmanTransactionsHistory(input) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_salesman_transactions_history/'+ 
        input.selectedSalesmanId + '/' +
        input.transactionType + '/' +
        input.pageNum + '/' + 
        input.transactionsPerPage
        , this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getRetailerAllTransactions(retailer_id) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_retailer_all_transactions/' + retailer_id, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getRetailerTransactionsInYear(retailer_id, year) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_retailer_transactions_in_year/' + retailer_id + '/' + year, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getRetailerTransactionsInMonth(retailer_id, year, month) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_retailer_transactions_in_month/' + retailer_id + '/' + year + '/' + month, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getChainAllTransactions() {
    //57f91b40a4902d8c4ae54324
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_chain_all_transactions', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getChainTransactionsInYear(year) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_chain_transactions_in_year/' + year, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getChainTransactionsInMonth(year, month) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_chain_transactions_in_month/' + year + '/' + month, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getSalesmanAllTransactions(salesman_id) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_salesman_all_transactions/' + salesman_id, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getSalesmanTransactionsInYear(salesman_id, year) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_salesman_transactions_in_year/' + salesman_id + '/' + year, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getSalesmanTransactionsInMonth(salesman_id, year, month) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/chain/get_salesman_transactions_in_month/' + salesman_id + '/' + year + '/' + month, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
// ==============================================================
// =                          Misc                              =
// ==============================================================

  getToken(): any{
    return this.helperService.getToken();
  }
  
  getOptions(){
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.getToken()
    });
    return new RequestOptions({ headers: headers });
  }

  getOptionsWithAuth(authToken){
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });
    return new RequestOptions({ headers: headers });
  }

  logout(){
    this.helperService.logout();
  }
}
