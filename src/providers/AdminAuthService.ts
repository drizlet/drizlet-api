import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
//import {ObservingComponent} from '../providers/ObservingComponent'

import {HelperService} from "./HelperService";

import {API_ENDPOINT} from "../providers/constants"
import 'rxjs/add/operator/map';

//import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class AdminAuthService {
  
  /*
  // Observable source
  private _adminAuthSource = new BehaviorSubject<number>(0);
  // Observable navItem stream
  adminAuthObservable$ = this._adminAuthSource.asObservable();
  // service command
  
  sendResponse(res) {
    this._adminAuthSource.next(res);
  }
  */

  constructor(private http: Http, private helperService: HelperService) {
    this.http = http;
    this.helperService = helperService;
    //this.observingComponent = observingComponent;
  }

  register(user) {
  
    var isloginEmpty = false;
    var isPasswordEmpty = false;
    var isEmailEmpty = false;
    
    if(!user.username || user.username === '' || user.username.length === 0){
      isloginEmpty = true;
    }

    if(!user.password || user.password === '' || user.password.length === 0){
      isPasswordEmpty = true;
    }
    
    if(!user.email || user.email === '' || user.email.length === 0){
      isEmailEmpty = true;
    }
    
    return new Promise((resolve, reject) => {
      if(isloginEmpty || isPasswordEmpty || isEmailEmpty){
        reject('Please enter required fields!');
      }
      else {
        this.http.post(API_ENDPOINT.url + '/admin/signup', user, this.getOptions())
        .map(res => {
          /*
          if(res.status < 200 || res.status >= 300) {
            reject('This request has failed ' + res.status);
          } 
          */
          return res.json();
        })
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('admin', result.token);
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  login(user) {
    var isloginEmpty = false;
    var isPasswordEmpty = false;
    if(!user.login || user.login === '' || user.login.length === 0){
      isloginEmpty = true;
    }

    if(!user.password || user.password === '' || user.password.length === 0){
      isPasswordEmpty = true;
    }
    
    return new Promise((resolve, reject) => {
      if(isloginEmpty && isPasswordEmpty){
        reject('Username and password not entered!');
      }
      else if(isloginEmpty){
        reject('Username not entered!');
      }
      else if(isPasswordEmpty){
        reject('Password not entered!');
      }
      else {
        
        this.http.post(API_ENDPOINT.url + '/admin/login', user)
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('admin', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  login_with_auth(user) {
    return new Promise((resolve, reject) => {
      if(!user.authToken || user.authToken === '' || user.authToken.length === 0){
        reject('No authentication found!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/admin/login_auth', this.getOptionsWithAuth(user.authToken))
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('admin', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  login_with_token() {
    return new Promise((resolve, reject) => {
      this.helperService.loadUserCredentials();
      if(!this.getToken() || this.getToken() === '' || this.getToken().length === 0){
        reject('No authentication found!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/admin/login_auth', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            this.helperService.storeUserCredentials('admin', result.token);
            resolve(result.success);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  getInfo() {
    return new Promise((resolve, reject) => {
      if(!this.getToken()){
        reject('token does not exist!');
      }
      this.http.get(API_ENDPOINT.url + '/admin/info', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result.data);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getUsers() {
    return new Promise((resolve, reject) => {
      if(!this.getToken()){
        reject('token does not exist!');
      }
      this.http.get(API_ENDPOINT.url + '/admin/get_users', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result.data);
        } else {
          reject(result.data);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getUserInfo(user_id) {
    return new Promise((resolve, reject) => {
      if(!user_id){
        reject('User id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/admin/get_user_info/'+user_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };

  update(user) {
    return new Promise((resolve, reject) => {
      var isloginEmpty = false;
      var isPasswordEmpty = false;
      var isEmailEmpty = false;
      
      if(!user.username || user.username === '' || user.username.length === 0){
        isloginEmpty = true;
      }

      if(!user.password || user.password === '' || user.password.length === 0){
        isPasswordEmpty = true;
      }
      
      if(!user.email || user.email === '' || user.email.length === 0){
        isEmailEmpty = true;
      }
    
      if(isloginEmpty || isPasswordEmpty || isEmailEmpty){
        reject('Please enter required fields!');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/admin/update', user, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  changePassword(passwords) {
    return new Promise((resolve, reject) => {
      var isOldEmpty = false;
      var isNewEmpty = false;
      var isRepeatEmpty = false;
      
      if(!passwords.old || passwords.old === '' || passwords.old.length === 0){
        isOldEmpty = true;
      }

      if(!passwords.new || passwords.new === '' || passwords.new.length === 0){
        isNewEmpty = true;
      }

      if(!passwords.repeat || passwords.repeat === '' || passwords.repeat.length === 0){
        isRepeatEmpty = true;
      }

      if(isOldEmpty || isNewEmpty || isRepeatEmpty){
        reject('Please enter all details.');
      }
      else if(passwords.new !== passwords.repeat){
        reject('New passwords do not match.');
    }
      else if(passwords.old === passwords.repeat){
        reject('Same password entered.');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/admin/change_password', passwords, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }


// ==============================================================
// =                       Retailer                             =
// ==============================================================
  addNewRetailer(retailer) {
    return new Promise((resolve, reject) => {
      if(!retailer){
        reject('Error in Retailer data.');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/admin/add_new_retailer', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  getRetailers() {
    return new Promise((resolve, reject) => {
        this.http.get(API_ENDPOINT.url + '/admin/get_retailers', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  getRetailerInfo(retailer_id) {
    return new Promise((resolve, reject) => {
      if(!retailer_id){
        reject('Retailer id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/admin/get_retailer_info/'+retailer_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
  updateRetailerProfile(retailer) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/update_retailer_profile', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  updateRetailerDeals(retailer) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/update_retailer_deals', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  changeRetailerPassword(retailer){
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/change_retailer_password', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  }

  resetRetailerDeals(retailer){
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/reset_retailer_deals', retailer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  }

// ==============================================================
// =                       Cutomers                             =
// ==============================================================
addNewCustomer(customer) {
    return new Promise((resolve, reject) => {
      if(!customer){
        reject('Error in Customer');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/admin/add_new_customer', customer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  getCustomers() {
    return new Promise((resolve, reject) => {
        this.http.get(API_ENDPOINT.url + '/admin/get_customers', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  getCustomerInfo(customer_id) {
    return new Promise((resolve, reject) => {
      if(!customer_id){
        reject('Customer id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/admin/get_customer_info/'+customer_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
  updateCustomerProfile(customer) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/update_customer_profile', customer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  changeCustomerPassword(customer){
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/change_customer_password', customer, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  }


// ==============================================================
// =                       Salesman                             =
// ==============================================================
addNewSalesman(salesman) {
    return new Promise((resolve, reject) => {
      if(!salesman){
        reject('Error in Salesman');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/admin/add_new_salesman', salesman, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  getSalesmen() {
    return new Promise((resolve, reject) => {
        this.http.get(API_ENDPOINT.url + '/admin/get_salesmen', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  getSalesmanInfo(salesman_id) {
    return new Promise((resolve, reject) => {
      if(!salesman_id){
        reject('Salesman id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/admin/get_salesman_info/'+salesman_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
  updateSalesmanProfile(salesman) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/update_salesman_profile', salesman, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  changeSalesmanPassword(salesman){
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/change_salesman_password', salesman, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  }

// ==============================================================
// =                        Chains                              =
// ==============================================================
addNewChain(chain) {
    return new Promise((resolve, reject) => {
      if(!chain){
        reject('Error in Chain');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/admin/add_new_chain', chain, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  getChains() {
    return new Promise((resolve, reject) => {
        this.http.get(API_ENDPOINT.url + '/admin/get_chains', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  getChainInfo(chain_id) {
    return new Promise((resolve, reject) => {
      if(!chain_id){
        reject('Chain id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/admin/get_chain_info/'+chain_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
  updateChainProfile(chain) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/update_chain_profile', chain, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  changeChainPassword(chain){
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/change_chain_password', chain, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  }


// ==============================================================
// =                       Drizlet Card                         =
// ==============================================================
  addNewDrizletCard(drizletCard) {
    return new Promise((resolve, reject) => {
      if(!drizletCard){
        reject('Error in Drizlet Card data.');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/admin/add_new_drizletcard', drizletCard, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  addNewDrizletCardRange(drizletCard) {
    return new Promise((resolve, reject) => {
      if(!drizletCard){
        reject('Error in Drizlet Card data.');
      }
      else if (!drizletCard.card_code_start){
        reject('Drizlet Card start not entered.');
      }
      else if (!drizletCard.card_code_end){
        reject('Drizlet Card end not entered.');
      }
      else if (!drizletCard.key){
        reject('Key not entered.');
      }
      else{
        this.http.post(API_ENDPOINT.url + '/admin/add_new_drizletcard_range', drizletCard, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result.msg);
          } else {
            reject(result.msg);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  }

  getDrizletCards() {
    return new Promise((resolve, reject) => {
        this.http.get(API_ENDPOINT.url + '/admin/get_drizletcards', this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  getDrizletCardInfo(drizletcard_id) {
    return new Promise((resolve, reject) => {
      if(!drizletcard_id){
        reject('Drizlet Card id not entered!');
      }
      else {
        this.http.get(API_ENDPOINT.url + '/admin/get_drizletcard_info/'+drizletcard_id, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      }
    });
  };
  
  updateDrizletCardUserID(drizletcard) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/update_drizletcard_user_id', drizletcard, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  addDrizletcardPoints(drizletcard) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/add_drizletcard_points', drizletcard, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  updateDrizletcardDeals(drizletcard) {
    return new Promise((resolve, reject) => {
      this.http.post(API_ENDPOINT.url + '/admin/update_drizletcard_deals', drizletcard, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  subtractDrizletcardPoints(drizletcard) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/subtract_drizletcard_points', drizletcard, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };

  setDrizletcardMembership(drizletcard) {
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/set_drizletcard_membership', drizletcard, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  };
  
// ==============================================================
// =                     Transactions                           =
// ==============================================================

  createTransaction(){
    var transaction = {
      chain_id: '5804088bdb45bf302114be58',
      retailer_id: '57f91b40a4902d8c4ae54324',
      customer_id: '123456789012',
      deal_type: 'points',
      deal_value: 30,
      transaction_type: 'out',
      salesman_id: '57c991a0946499712fb3ceeb'
    }
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/create_transaction', transaction, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  }

  create_dummy_transaction(){
    var transaction = {
      chain_id: '5804088bdb45bf302114be58',
      retailer_id: '57f91b40a4902d8c4ae54324',
      customer_id: '57c991be946499712fb3ceee',
      deal_type: 'points',
      deal_value: 30,
      transaction_type: 'in',
      salesman_id: '57c991a0946499712fb3ceeb'
    }
    return new Promise((resolve, reject) => {
        this.http.post(API_ENDPOINT.url + '/admin/create_dummy_transaction', transaction, this.getOptions())
        .map(res => res.json())
        .subscribe(result => {
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }, error => {
          console.error('error: ' + error);
          reject(this.helperService.formatHttpResult(error));
        });
      
    });
  }

  getAllTransactionsHistory(input) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_all_transactions_history/'+ 
        input.transactionType + '/' +
        input.dealsType + '/' + 
        input.pageNum + '/' + 
        input.transactionsPerPage
        , this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getChainTransactionsHistory(input) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_chain_transactions_history/'+ 
        input.selectedChainId + '/' +
        input.transactionType + '/' +
        input.dealsType + '/' + 
        input.pageNum + '/' + 
        input.transactionsPerPage
        , this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getRetailerTransactionsHistory(input) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_retailer_transactions_history/'+ 
        input.selectedRetailerId + '/' +
        input.transactionType + '/' +
        input.dealsType + '/' + 
        input.pageNum + '/' + 
        input.transactionsPerPage
        , this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getSalesmanTransactionsHistory(input) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_salesman_transactions_history/'+ 
        input.selectedSalesmanId + '/' +
        input.transactionType + '/' +
        input.dealsType + '/' + 
        input.pageNum + '/' + 
        input.transactionsPerPage
        , this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getNumTransactions() {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_num_transactions', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };


  getTransactionsComparison() {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_transactions_comparison', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getTransactionsPointsComparison() {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_transactions_points_comparison', this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getAllTransactions(deals_type) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_all_transactions' + '/' + deals_type, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getTransactionsInYear(year, deals_type) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_transactions_in_year/' + year + '/' + deals_type, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getTransactionsInMonth(year, month, deals_type) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_transactions_in_month/' + year + '/' + month + '/' + deals_type, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getRetailerAllTransactions(retailer_id) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_retailer_all_transactions/' + retailer_id, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getRetailerTransactionsInYear(retailer_id, year) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_retailer_transactions_in_year/' + retailer_id + '/' + year, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getRetailerTransactionsInMonth(retailer_id, year, month) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_retailer_transactions_in_month/' + retailer_id + '/' + year + '/' + month, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  getChainAllTransactions(chain_id) {
    //57f91b40a4902d8c4ae54324
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_chain_all_transactions/' + chain_id, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getChainTransactionsInYear(chain_id, year) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_chain_transactions_in_year/' + chain_id + '/' + year, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getChainTransactionsInMonth(chain_id, year, month) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_chain_transactions_in_month/' + chain_id + '/' + year + '/' + month, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getSalesmanAllTransactions(salesman_id) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_salesman_all_transactions/' + salesman_id, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getSalesmanTransactionsInYear(salesman_id, year) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_salesman_transactions_in_year/' + salesman_id + '/' + year, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };

  getSalesmanTransactionsInMonth(salesman_id, year, month) {
    return new Promise((resolve, reject) => {
      this.http.get(API_ENDPOINT.url + '/admin/get_salesman_transactions_in_month/' + salesman_id + '/' + year + '/' + month, this.getOptions())
      .map(res => res.json())
      .subscribe(result => {
        if (result.success) {
          resolve(result);
        } else {
          reject(result.msg);
        }
      }, error => {
        console.error('error: ' + error);
        reject(this.helperService.formatHttpResult(error));
      });
    });
  };
  
  
// ==============================================================
// =                          Misc                              =
// ==============================================================

  getToken(): any{
    return this.helperService.getToken();
  }

  getOptions(){
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.getToken()
    });
    return new RequestOptions({ headers: headers });
  }

  getOptionsWithAuth(authToken){
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });
    return new RequestOptions({ headers: headers });
  }

  logout(){
    this.helperService.logout();
  }
}
